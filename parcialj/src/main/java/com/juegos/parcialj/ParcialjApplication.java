package com.juegos.parcialj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParcialjApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParcialjApplication.class, args);
	}

}
