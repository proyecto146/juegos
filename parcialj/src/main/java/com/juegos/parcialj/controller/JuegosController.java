package com.juegos.parcialj.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.juegos.parcialj.dto.internal.CrearJuegosRequest;
import com.juegos.parcialj.model.Juegos;
import com.juegos.parcialj.service.JuegosService;

@RestController
@RequestMapping("/Juegos")
public class JuegosController {     

    @Autowired
    private JuegosService juegosService;

    @PostMapping(value = "/guardar")
    public ResponseEntity<Juegos> guardar (@Valid @RequestBody CrearJuegosRequest request ){      
        return ResponseEntity.ok(juegosService.create(request));

    }

    @GetMapping
    private ResponseEntity<List<Juegos>> listarJuegos (){
        return ResponseEntity.ok(juegosService.getAllJuegos());
    }

    @DeleteMapping("/eliminar/{id}")
    public String deleteById(@PathVariable("id") Integer id) {
       return juegosService.deleteJuegos(id);
    }

    @GetMapping  (value  = "/{id}")
    private ResponseEntity<Optional<Juegos>> listarJuegosId (@PathVariable ("id") Integer id){
        return ResponseEntity.ok(juegosService.findById(id));
    }


    @PutMapping (value = "/actualizar")
    public Juegos actualizJuegos(@RequestBody Juegos jue ) {
        
       return juegosService.update(jue);
    }


    
    
}  

