 package com.juegos.parcialj.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juegos.parcialj.dto.internal.CrearJuegosRequest;
import com.juegos.parcialj.model.Juegos;
import com.juegos.parcialj.repository.JuegosRepositorio;

@Service
public class JuegosService {
    

    @Autowired
    private JuegosRepositorio juegosRepositorio;

    public Juegos create (CrearJuegosRequest request) 
    {

    Juegos juegos = new Juegos();
    juegos.setDescripcion(request.getDescripcion());
    juegos.setNombre(request.getNombre());
        return juegosRepositorio.save(juegos);
    
    
    }

    public List<Juegos> getAllJuegos() {
        return juegosRepositorio.findAll();
    }

    public String deleteJuegos(Integer id){
        juegosRepositorio.deleteById(id);
        return "OK";
    };
    

    public Optional<Juegos> findById(Integer id) {
        return juegosRepositorio.findById(id);

    }

    public Juegos update (Juegos juegos){
        return juegosRepositorio.save(juegos);
    }  

}
