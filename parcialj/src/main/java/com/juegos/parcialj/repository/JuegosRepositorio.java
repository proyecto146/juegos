package com.juegos.parcialj.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.juegos.parcialj.model.Juegos;

public interface JuegosRepositorio extends JpaRepository<Juegos, Integer> {
    
}
