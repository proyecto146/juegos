package com.juegos.parcialj.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="JUEGOS", schema = "DBO")
public class Juegos {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name="id_juego")
    private Integer id;

    @Column (name="nombre_juego")
    private String nombre;

    @Column (name="descripcion_juego")
    private String descripcion;


}
