package com.juegos.parcialj.dto.internal;





import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CrearJuegosRequest {

    @NotNull(message = "El nombre del juego es requerido")
    @NotBlank(message = "El nombre del juego no puede estar en blanco")
    private String nombre;

    @NotNull(message = "La descripcion del juego es requerido")
    @NotBlank(message = "La descripcion del juego no puede estar en blanco")
    private String descripcion;
    
}
